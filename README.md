<!-- SPACY PROJECT: AUTO-GENERATED DOCS START (do not remove) -->

# 🪐 spaCy Project: Example project of creating a novel nlp component to do relation extraction from scratch.

This example project shows how to implement a spaCy component with a custom Machine Learning model, how to train it with and without a transformer, and how to apply it on an evaluation dataset.

## 📋 project.yml

The [`project.yml`](project.yml) defines the data assets required by the
project, as well as the available commands and workflows. For details, see the
[spaCy projects documentation](https://spacy.io/usage/projects).

### ⏯ Commands

The following commands are defined by the project. They
can be executed using [`spacy project run [name]`](https://spacy.io/api/cli#project-run).
Commands are only re-run if their inputs have changed.

| Command | Description |
| --- | --- |
| `data` | Parse the gold-standard annotations from the Prodigy annotations. |
| `train_cpu` | Train the REL model on the CPU and evaluate on the dev corpus. |
| `train_gpu` | Train the REL model with a Transformer on a GPU and evaluate on the dev corpus. |
| `evaluate` | Apply the best model to new, unseen text, and measure accuracy at different thresholds. |
| `clean` | Remove intermediate files to start data preparation and training from a clean slate. |

### ⏭ Workflows

The following workflows are defined by the project. They
can be executed using [`spacy project run [name]`](https://spacy.io/api/cli#project-run)
and will run the specified commands in order. Commands are only re-run if their
inputs have changed.

| Workflow | Steps |
| --- | --- |
| `all` | `data` &rarr; `train_cpu` &rarr; `evaluate` |
| `all_gpu` | `data` &rarr; `train_gpu` &rarr; `evaluate` |

### 🗂 Assets

The following assets are defined by the project. They can
be fetched by running [`spacy project assets`](https://spacy.io/api/cli#project-assets)
in the project directory.

| File | Source | Description |
| --- | --- | --- |
| [`assets/annotations.jsonl`](assets/annotations.jsonl) | Local | Gold-standard REL annotations created with Prodigy |

<!-- SPACY PROJECT: AUTO-GENERATED DOCS END (do not remove) -->
<br>
<br>
<br>

#  Penjelasan mengenai data preprocessing 
Pada proses pembuatan model relation ekstraction menggunakan spacy v3, text harus diubah ke data class Doc spacy

Contoh:

Misal kita punya data seperti berikut

`" \<e1> Semarang \</e1> has some medical schools that offer school of medicine nursing etc. such as Faculty of Medicine \<e2> Diponegoro University \</e2> Faculty of Medicine UNISSULA and Faculty of Medicine UNIMUS . "` <br>
`org:subsidiaries(e1,e2)`

dan jenis2 relasi yang ingin diklasifikasi seperti berikut

1. `city_of_headquarters`
2. `subsidiaries`
3. `alternate_names`

Data text diatas mempunyai 2 entitas, yaitu `Semarang` dan `Diponegoro` university dengan relasi subsidiaries arah e1 ke e2

Maka data diatas harus diubah ke dalam spacy doc format dengan cara

```
words = ['Semarang', 'has', 'some', 'medical', 'schools', 'that', 'offer', 'school', 'of', 'medicine', 'nursing', 'etc', '.', 'such', 'as', 'Faculty', 'of', 'Medicine', 'Diponegoro',  'University', 'Faculty', 'of', 'Medicine', 'UNISSULA', 'and', 'Faculty', 'of', 'Medicine', 'UNIMUS', '.' ]

spaces = [True, True, True, True, True, True, True, True, True, True, True, False, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True, True]

doc = Doc(vocab, words=words, spaces=spaces)
```

variabel spaces' menyimpan boolean value "ada spasi di depan token".

Untuk mendapatkan `words` dan `spaces` dari text kalimat, silahkan dipelajari dokumentasi library [spacy](https://course.spacy.io/en).

Kemudian kita juga harus menyimpan data entitasnya, yaitu dengan cara dimana untuk setiap entitasnya kita lakukan proses berikut

```
entities = []

# disini lakukan looping untuk setiap entitas
    entity =  doc.char_span(start_char, end_char, label=ner_label)
    entities.append(entity)

# setelah menyimpan semua data entitas ke var entities, simpan entities ke doc.ents

doc.ents = entities
```

- `start_char`: index awal string entitas
- `end_char`: offset index akhir string entitas 
- `ner_label`: label ner dari entitas

Selanjutnya data relasi juga disimpan ke dalam format dictionary berikut

```
{(x1, x2): {"city_of_headquarters": 1.0, "subsidiaries": 0.0, "alternate_names": 0.0}}
```
 - `x1`:  index awal string entitas 1
 - `x2`:  index awal string entitas 2

Menggunakan contoh data diatas (misal start char entitas `Semarang` = 0,  start char entitas `Diponegoro University` = 58 ), maka data relasi mempunyai bentuk

```
rels = {(0, 58):{
                    "city_of_headquarters": 0.0, 
                    "subsidiaries": 1.0, 
                    "alternate_names": 0.0
                }
        (58, 0):{
                    "city_of_headquarters": 0.0, 
                    "subsidiaries": 0.0, 
                    "alternate_names": 0.0
                }                       
        }
```
Setelah itu var `rels` disimpan ke `doc._.ents`

```
doc._.rel = rels
```

Catatan: extension `rel` pada `doc` tidak ada secara default, sehingga harus ditambahkan terlebih dahulu di awal 

```
Doc.set_extension("rel", default={})
```
Tiap data yang telah kita simpan ke dalam class Doc beserta data entitas dan relasinya, tiap data doc tersebut kita simpan ke dalam `list`
```
docs = []

# Proses memformat data kedalam doc
# |
# |

docs.append(doc)
```
Selanjutnya list `docs` kita taruh ke dalam object `DocBin` dan disimpan ke dalam file dengan alamat `save_path`

```
docbin = DocBin(docs=docs, store_user_data=True)
docbin.to_disk(save_path)
```

Untuk lebih detail mengenai data preprocessing silahkan dipelajari script python berikut
1. `custom_parse_data_kpb37_ver2.py`
2. `parse_data.py`

# Modifikasi model 

model dapat dimodifikasi padadengan mengedit `rel_model.py` dalam folder script pada bagian fungsi `create_classification_layer`
pada bagian return silahkan diedit sesuai dengan struktur model yang diinginkan, model neural network pada spacy menggunakan library [Thinc](https://thinc.ai/)

# Menjalankan proses preprocessing data dan Training

Setelah melakukan editing yang diperlukan, dapat menjalankan sesuai dengan instruksi diawal dengan terlebih dahulu mengedit file `project.yml` agar sesuai dengan perubahan yang telah dilakukan atau dapat menjalankan tiap proses secara manual 

Pada terminal jalankan perintah-perintah berikut 

1. Processing data
```
python ./scripts/custom_parse_data_kpb37_ver2.py 
```

2. training data

Menggunakan token2vec

```
spacy project run train_cpu
```
atau kalau menggunakan transformer BERT
```
spacy project run train_gpu
```

3. Evaluasi

```
spacy project run evaluate
```

# Lain-lain

Bisa juga eksperimen dengan project versi awal dengan mendownload repo versi awal

Pada terminal di path folder kosong, jalankan perintah

```
python -m spacy project clone tutorials/rel_component
```