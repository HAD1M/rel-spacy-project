import json

import typer
from pathlib import Path

import spacy

from spacy.tokens import Span, DocBin, Doc
from spacy.vocab import Vocab
from wasabi import Printer
import pandas as pd

from spacy.lang.en import English
from spacy.matcher import PhraseMatcher

import numpy as np
from nltk.tokenize import word_tokenize

msg = Printer()
LABEL_LIST = ["per:employee_of", "org:city_of_headquarters", "org:members", "org:founded_by", "org:country_of_headquarters", "org:subsidiaries",
              "per:stateorprovinces_of_residence", "per:cities_of_residence", "no_relation", "per:countries_of_residence", "org:stateorprovince_of_headquarters",
              "per:title", "org:top_members/employees", "org:founded", "org:alternate_names", "per:spouse", "per:country_of_birth", "per:alternate_names", 
              "per:origin"]


def tokens_diff(sentence1, sentence2):
    
    token_list_1 = np.array(word_tokenize(sentence1))
    token_list_2 = np.array(word_tokenize(sentence2))
    
    return len(token_list_1[np.in1d(token_list_1, token_list_1)])/np.amax([len(token_list_1), len(token_list_2)])


def parse_data(data_loc: Path, save_path: Path):

    docs = []

    nlp = spacy.load("en_core_web_trf")
    vocab = Vocab()

    data = pd.read_csv(data_loc)

    for column in data.columns:
        data[column] = data[column].apply(lambda x: x.strip())

    for idx in range(len(data)):
        text = data["text_clean"][idx]
        rel_label = data["label_clean"][idx]
        e1 = data["e1_clean"][idx]
        e2 = data["e2_clean"][idx]
        neg = 0
        pos = 0

        try:

            example = nlp(text)

            span_starts = set()
            words = [token.text for token in example]
            spaces = [token.whitespace_==' ' for token in example]
            doc = Doc(vocab, words=words, spaces=spaces)

            pattern_e1 = nlp(e1)
            pattern_e2 = nlp(e2)

            matcher = PhraseMatcher(vocab)

            matcher.add(e1, None, pattern_e1)
            matcher.add(e2, None, pattern_e2)

            entities=[]
            span_end_to_start = {}
            ent_label = {}


            for ent in example.ents:
                ent_label[ent.text] = ent.label_
                
            for match_id, start, end in matcher(example):
                # Get the matched span
                span = example[start:end]
                
                try:
                    span_label = ent_label[span.text]
                except:
                    span_label = None
                    
                if span_label is None:
                    try:
                        closer_score = {}
                        for ent in example.ents:
                            closer_score[tokens_diff(span.text, ent.text)] = ent.text
                        span_label=ent_label[closer_score[np.amax(list(closer_score.keys()))]]
                    except:
                        span_label = "Unknown"
                    
                entity = doc.char_span(span.start_char, span.end_char, label=span_label)
                span_end_to_start[end-1] = start
                entities.append(entity)
                span_starts.add(start)         

            doc.ents = entities

            rels = {}
            for x1 in span_starts:
                for x2 in span_starts:
                    rels[(x1, x2)] = {}

            for ent1, x1 in zip(entities, span_starts):
                for ent2, x2 in zip(entities, span_starts):
                    if ent1.text != ent2.text:
                        rels[(x1, x2)][rel_label] = 1.0
                        pos += 1
                        
            for x1 in span_starts:
                for x2 in span_starts:
                    for label in LABEL_LIST:
                        if label not in rels[(x1, x2)]:
                            neg += 1
                            rels[(x1, x2)][label] = 0.0
                            
            doc._.rel = rels

            docs.append(doc)

        except:
            msg.fail(f"Skipping doc because error in doc with index {idx}") 

    docbin = DocBin(docs=docs, store_user_data=True)
    docbin.to_disk(save_path)

def main():

    Doc.set_extension("rel", default={})

    train_data_loc = Path("./assets/data_kpb37/Clean/kbp37_clean_v2.csv")
    dev_data_loc = Path("./assets/data_kpb37/Clean/kbp37_dev.csv")
    test_data_loc = Path("./assets/data_kpb37/Clean/kbp37_test.csv")

    train_file= Path("./data/train.spacy")
    dev_file= Path("./data/dev.spacy") 
    test_file= Path("./data/test.spacy")


    parse_data(train_data_loc, train_file)
    msg.info("Train Data Success")
    parse_data(dev_data_loc, dev_file)
    msg.info("Dev Data Success")
    parse_data(test_data_loc, test_file)
    msg.info("Test Data Success")

    msg.info("All Success")


if __name__ == "__main__":
    typer.run(main)