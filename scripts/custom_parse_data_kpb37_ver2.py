import json

import typer
from pathlib import Path

import spacy

from spacy.tokens import Span, DocBin, Doc
from wasabi import Printer

from spacy.lang.en import English
from spacy.matcher import Matcher, PhraseMatcher

import numpy as np

import pickle
import re
import json

LABEL_LIST = ['alternate_names', 'cities_of_residence', 'city_of_headquarters', 
              'countries_of_residence', 'country_of_birth', 'country_of_headquarters', 'employee_of', 'founded', 
              'founded_by', 'members', 'no_relation', 'origin', 'spouse', 'stateorprovince_of_headquarters', 
              'stateorprovinces_of_residence', 'subsidiaries', 'title', 'top_members/employees']

msg = Printer()
nlp = spacy.load("en_core_web_trf")
vocab = nlp.vocab
Doc.set_extension("rel", default={})

def parse_data(data_loc: Path, save_path: Path):

    docs = []
    idx = 0
    failed = 0
    with data_loc.open("r", encoding="utf8") as jsonfile:
        for line in jsonfile:
            try:
                example = json.loads(line)

                span_starts = set()
                words = example["words"]
                spaces = example["spaces"]
                doc = Doc(vocab, words=words, spaces=spaces)

                neg = 0
                pos = 0

                # Parse the entities
                spans = example["entities"]

                entities = []
                span_end_to_start = {}

                entity_1 = spans["entity_1"]
                entity = doc.char_span(entity_1["start_char"], entity_1["end_char"], label=entity_1["label"])
                entities.append(entity)
                span_end_to_start[1] = entity_1["token_start"]
                span_starts.add(entity_1["token_start"])

                #print(entity_1["text"], " ", entity.text)

                assert entity_1["text"] == entity.text

                entity_2 = spans["entity_2"]
                entity = doc.char_span(entity_2["start_char"], entity_2["end_char"], label=entity_2["label"])
                entities.append(entity)
                span_end_to_start[2] = entity_2["token_start"]
                span_starts.add(entity_2["token_start"])

                #print(entity_2["text"], " ", entity.text)

                assert entity_2["text"] == entity.text

                doc.ents = entities

                # Parse the relations
                rels = {}
                relations = example["relations"]
                rel_label = relations["label"].strip()

                for x1 in span_starts:
                    for x2 in span_starts:
                        rels[(x1, x2)] = {}

                if rel_label == 'no_relation':
                    rels[(span_end_to_start[1], span_end_to_start[2])][rel_label] = 1.0
                    rels[(span_end_to_start[2], span_end_to_start[1])][rel_label] = 1.0
                    pos += 1
                else:
                    if relations["direction"] == "R":
                        rels[(span_end_to_start[1], span_end_to_start[2])][rel_label] = 1.0
                        pos += 1
                    elif relations["direction"] == "L":
                        rels[(span_end_to_start[2], span_end_to_start[1])][rel_label] = 1.0 
                        pos += 1
                    else:
                        rels[(span_end_to_start[1], span_end_to_start[2])]['no_relation'] = 1.0
                        rels[(span_end_to_start[2], span_end_to_start[1])]['no_relation'] = 1.0        
                        pos += 1
                            
                for x1 in span_starts:
                    for x2 in span_starts:
                        for label in LABEL_LIST:
                            if label not in rels[(x1, x2)]:
                                neg += 1
                                rels[(x1, x2)][label] = 0.0
            
                doc._.rel = rels

                docs.append(doc)
                idx+=1

            except:
                msg.fail(f"Skipping doc because error in doc with index {idx}")
                idx+=1
                failed+=1

    msg.warn(f"Total loaded data fail: {failed}, percentage: {failed/idx * 100} %")
    docbin = DocBin(docs=docs, store_user_data=True)
    docbin.to_disk(save_path)


def main():

    train_data_loc = Path("./assets/data_kpb37/Cleaned_v21/kbp37train.jsonl")
    dev_data_loc = Path("./assets/data_kpb37/Cleaned_v21/kbp37dev.jsonl")
    test_data_loc = Path("./assets/data_kpb37/Cleaned_v21/kbp37test.jsonl")

    train_file= Path("./data/v1/train.spacy")
    dev_file= Path("./data/v1/dev.spacy") 
    test_file= Path("./data/v1/test.spacy")


    parse_data(train_data_loc, train_file)
    msg.info("Train Data Success")
    parse_data(dev_data_loc, dev_file)
    msg.info("Dev Data Success")
    parse_data(test_data_loc, test_file)
    msg.info("Test Data Success")

    msg.info("All Success")


if __name__ == "__main__":
    typer.run(main)